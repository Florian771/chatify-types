export declare namespace Node {
  export interface IFolder {
    name: string
    path: string
    foldersCount?: number
    filesCount?: number
    logInfo?: string
  }
  export interface IFile {
    name: string
    path: string
  }
}
