export declare namespace MarketPlace {
  export interface Item {
    id: string
    categoryId?: string
    category: string
    name: string
    company: string
    downloaded: number
    installed: boolean
    installing: boolean
  }
}
