export declare namespace MachineLearning {
  export interface IPrediction {
    className: string
    probability: number
  }
}
