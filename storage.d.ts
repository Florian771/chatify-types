export interface StorageFile {
  bucket: BucketClass | string
  storage?: Storage
  name: string
  acl?: ACL
  kind?: string
  id?: string
  selfLink?: string
  generation?: string
  metageneration?: string
  contentType?: string
  timeCreated?: string
  updated?: string
  storageClass?: string
  timeStorageClassUpdated?: string
  size?: string
  md5Hash?: string
  mediaLink?: string
  crc32c?: string
  etag?: string
}

export interface ACL {
  owners: Storage
  readers: Storage
  writers: Storage
  pathPrefix: string
  default?: ACL
}

export interface Storage {}

export interface BucketClass {
  name: string
  storage: Storage
  acl: ACL
  iam: Iam
}

export interface Iam {
  resourceId_: string
}

export interface StorageUploadInfo {
  kind: string
  id: string
  selfLink: string
  name: string
  bucket: string
  generation: string
  metageneration: string
  contentType: string
  timeCreated: string
  updated: string
  storageClass: string
  timeStorageClassUpdated: string
  size: string
  md5Hash: string
  mediaLink: string
  crc32c: string
  etag: string
}
export as namespace cloudStorage
