import { YoloBoxInfo } from "."
import { admin } from "../chatify-utils-server/src/services/fb-functions"

export declare namespace ChatifyChat {
  export interface IChatProps {
    messagesLength?: number
    listening?: boolean
    translationSettings?: string
    size?: chatify.ChatSize
    showHeader?: boolean
    speakMode: boolean
    messages?: chatify.MessageObject[]
    debug?: boolean
    inputTextValue?: string
    speechRecognitionAvailable?: boolean
    speechSynthesisAvailable?: boolean
    hasShadow?: boolean
    sendButton?: boolean
    supportedTranslationLanguage?: chatify.TranslationLanguage[]
    showClearButton?: boolean
    showMicButton?: boolean
    showResendButton?: boolean
    showSpeakerButton?: boolean
    audio?: boolean
    htmlAudioElement?: React.RefObject<HTMLAudioElement>
    chatBoxStylesFullscreen?: React.CSSProperties
    translate(id: string, vars?: chatify.TranslationVars[]): string
    handleSubmit?(event: any): void
    handleTakePhoto?(): void
    handleAddAttachment?(): void
    handleChangeInputTextValue?(event: any): void
    handleChangeTranslation?(): void
    setRefInput?(ref: any): void
    clear?(): void
    reSend?(): void
    handleClickChatButton?(event: any): void
    handleSpeak?: () => void
    handleMicIconClick?(): void
    setChatSize?(size: "small" | "medium" | "maximum"): void
    setAudio?: (ref?: React.RefObject<HTMLAudioElement>) => void
  }

  export interface IGetConversationDbPath {
    userId: string
    projectId: string
  }
  export interface ISendMessage {
    rdb: admin.database.Database | firebase.database.Database
    message: chatify.MessageObject
    authToken: string
  }
  export interface IAppProps extends IChatSettings {
    containerCss?: any
    containerCssHeight?: string
    containerCssWidth?: string
    chat?: IChatProps
    projectId?: string | null
    initialState?: IAppState
    firebaseInitialized?: boolean
  }
  export interface IAppContext {
    state: IAppState
    dispatch(args: chatify.IAction): void
  }
  export interface IChatSettings {
    user?: firebase.User
    authToken?: string
    userSettings?: chatify.UserSettings
    projectSettings?: chatify.Project
    chat?: IChatProps
    chatSession?: string
    projectId?: string | null
    messagesLength?: number
    conversationStarted?: boolean
    chromeExtensionType?: string
  }

  export interface IAppState extends IChatSettings {
    language: "de" | "en"
    translation?: chatify.Translation[]
    conversationMessagesDbPath?: string
    chat?: IChatProps
    speechStore: chatify.ChatifyChat.SpeechClass
    translationSettings?: string
    lastMessage?: ISendMessage
    messageListener?: boolean
    message?: chatify.MessageObject
    screen?: "takePhoto" | "addAttachment"
    loading?: boolean
    webcamYoloBoxes?: YoloBoxInfo[]
    webcamShow?: boolean
  }

  export type SpeechSynthesisExtended = SpeechSynthesis & {
    onerror?: any
  }

  export class SpeechClass {
    progressIndex: number
    sync: SpeechSynthesisExtended
    speechSynthesisAvailable: boolean
    speechRecognitionAvailable: boolean
    textContent: string
    voices: SpeechSynthesisVoice[]
    selectedVoice: number
    speakMode: boolean
    recognition: SpeechRecognition
    speaker: SpeechSynthesisUtterance
    speechRecognition: SpeechRecognitionStatic
    constructor()
    setSpeaker: (text?: string) => void
    checkSpeech: () => boolean
    setTextContent(text: string): void
    initializeVoicePopulation: () => void
    changeSpeakMode: (val: boolean) => void
    getVoices(): SpeechSynthesisVoice[]
    setVoice(i: number): void
    fitVoiceToLanguage: (language: string) => void
    speak: (
      {
        text,
        language
      }: {
        text: string
        language: string
      }
    ) => void
    updateProgressIndex(): void
  }
}
