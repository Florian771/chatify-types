export declare namespace Communicator {
  export interface IContext {
    dispatch(args: Action): void
    translationStore: chatify.TranslationStore
  }
  export interface Channel {
    id: string
    name: string
    notification: boolean
  }
  export interface ICompany {
    id: string
    name: string
    homepage: string
    joined: boolean
  }
  export interface TeamMember {
    id: string
    name: string
    avatar: string
    follow: boolean
  }
  export interface Action {
    type: string
    payload?: any
  }
  export interface State {
    company: ICompany
    team: TeamMember[]
    channels: Channel[]
    currentChannelIndex: number
    threads: chatify.ChatMessage[]
    searchText?: string
    conversation?: chatify.ChatMessage[]
    conversationOpened?: boolean
  }
}
