import * as dialogflow from "dialogflow"
import { AxiosResponse, AxiosPromise } from "axios"
import { StringifyOptions } from "querystring"
import * as cloudStorage from "./storage"
import * as firebase from "firebase"
import { Chart } from "chart.js"
import Storage from "google-cloud__storage"
import * as ChatifyChat from "./Chat"

export * from "./Communicator"
export * from "./MarketPlace"
export * from "./Chat"
export * from "./MachineLearning"
export * from "./Node"
export * from "./Router"

export interface Intent extends dialogflow.Intent {
  dialogflowId?: string
  dialogflowProjectId?: string
  dialogflowSynced?: boolean
  _dialogflowSyncing?: boolean
  _id?: string
  _error?: string
  _status?: string
  _success?: string
  _movedToId?: string
  _nouns?: string[]
  _answerSummaries?: string[]
  _synonyms?: string[]
  answerPhrases?: dialogflow.TrainingPhrase[]
  importTimestamp?: string
  importTimestampId?: string
}

export interface BotAnswerCustomPayloadLink {
  title?: string
  image?: string
  url?: string
  video?: string
}
export interface BotAnswerCustomPayload {
  link?: BotAnswerCustomPayloadLink
}

export interface FileUploadResult {
  status: number
  success?: boolean
  uploadedFiles?: any
  allFiles?: chatify.File[]
  error?: any
}

export interface IVisionImageDescriptionLabel {
  description: string
}
export interface IVisionImageDescription {
  [key: string]: any
  labelAnnotations: IVisionImageDescriptionLabel[]
}

export interface YoloBoxInfo {
  left: number
  top: number
  rightLeft: number
  bottomTop: number
  className: string
  classProb: number
}

export interface IMenuItem {
  name: string
  icon: string
  link: string
  event: string
  badge?: number
  active?: boolean
  userRole?: chatify.UserRoles
}

export interface Error {
  code: number
  message: string
}

export interface EventTarget {
  name: string
  value: any
  type?: string
}
export interface Event {
  target: EventTarget
}

export type Breakpoint = "xs" | "sm" | "md" | "lg" | "xl"

export type NoticeLayout = "layoutImageLeft" | null
export interface Notice {
  _id?: string
  timestamp_created?: number
  title: string
  content?: string
  deleteable?: boolean
  refreshPageButton?: boolean
  images?: string[]
  layout?: NoticeLayout
}

/* CHAT */
export interface ChatMessage {
  id?: string
  language?: string
  firstMessage?: boolean

  private?: boolean

  channelId?: string
  channelName?: string

  from: ChatUser
  to: ChatUser
  txt: string

  botDoesntKnow?: string
  speakMode?: boolean

  upVotes?: number
  downVotes?: number
  answers?: number

  timestamp?: number
  date?: string

  queryText?: string
  allRequiredParamsPresent?: false
  diagnosticInfo?: any
  fulfillmentMessages?: dialogflow.Message[][]
  fulfillmentText?: string
  intent?: string | null
  intentDetectionConfidence?: number
  languageCode?: string
  outputContexts?: dialogflow.Context[]
  parameters?: dialogflow.Parameter[] | null // wahrscheinlich ein any array
  speechRecognitionConfidence?: number
  webhookPayload?: any
  webhookSource?: string

  speak?(response: string): void
}
export interface ChatUser {
  id: string
  name?: string
  company?: string
  position?: string
  tel?: string
  email?: string
  userRole?: string
  verified?: boolean
  rating?: boolean
}
/* STORES */
export interface Stores {}
/* STORES WEB */
export interface StoresWeb extends Stores {
  fcmStore: FcmStore
}
/* STORES Native */
export interface StoresNative extends Stores {
  routing?: any
  userStore: UserStoreNative
  fcmStore?: FcmStoreNative
  uiSettings: UiSettingsNative
  speechStore?: SpeechStoreNative
}
export interface UserStoreNative {}
export interface FcmStoreNative {}
export interface UiSettingsNative {}
export interface SpeechStoreNative {}

export interface DialogflowUnsolvedStore {
  loading: boolean
  unsolved: dialogflow.Intent[]
  active: dialogflow.Intent[]
  updateUnsolved(obj: dialogflow.Intent): void
  setActive(id: string): void
  get(id: string): void
  setUnsolved(data: dialogflow.Intent[], id: string): void
}
export interface FcmStore {
  token: string
  setTokenSentToServer: boolean
  permission: boolean
  getToken(token: string): void
}
export interface SearchStore {
  searching: boolean
  setSearching(value: boolean): void
}
export interface DialogflowChatStore {
  loading: boolean
  size: ChatSize // hidden,small,medium,maximum
  show: boolean
  user: ChatUser
  messages: chatify.ChatMessage[]
  setChatSize(type: string): void
  toggle(): void
  sendMessage(data: chatify.ChatMessage): void
}
export type ChatParent = "rightPanel" | "standalone" | "fullscreen"
export type ChatSize = "hidden" | "small" | "medium" | "maximum" | "contained"
export interface Translation {
  _id?: string
  key: string
  en: string
  de: string
  [key: string]: string
}

export interface TranslationVars {
  key: string
  value: string
}

export interface Translate {
  id: string
  vars?: TranslationVars
}
export interface TranslationStore {
  language: string
  translation: Translation[]
  loaded: boolean
  setLanguage(id: string): void
  get(id: string, vars?: TranslationVars[]): string
  load(language: string, callback?: () => void): void
}

export interface ProjectStore {
  currentProject: Project
  setCurrentProjectSettings({
    projectId,
    callback
  }: {
    projectId: string
    callback?(): void
  }): Promise<boolean>
}

/* Project */
export interface Project {
  id?: string
  _id?: string
  dialogflowProjectId?: string
  name: string
  clientId: string
  chatWelcomeMessage?: string
  chatAudioOutputEnabled?: boolean
  audioVoice?: string
}

/* FORM */
export interface FormDataRequestPermission {
  name?: string
}

export interface SuperAdmin {
  collection: string
  action: string
}
export interface Slot {
  displayName?: string
  entityTypeDisplayName?: string
  variable?: string
  prompts?: string[]
  newSlotPhrase: string
  filterText: string
}

export interface IntegrationFormUrl {
  url: string
  active: boolean
  primaryColor?: string
  secondaryColor?: string
  shadow?: boolean
}
export interface IntegrationFormDesign {
  primaryColor: string
  secondaryColor: string
  shadow: boolean
}
export interface IntegrationForm {
  _id?: string
  urls: IntegrationFormUrl[]
  design: IntegrationFormDesign
}
export interface PasswordResetForm {
  email: string
}
export interface DummyForm {
  _id?: string
  timestamp_created?: string
  timestamp_changed?: string
  name?: string
}
export type DbQueryOperators = "<" | "<=" | "==" | ">" | ">=" | "array_contains"
export type DbQueryFieldTypes =
  | "string"
  | "number"
  | "timestamp"
  | "boolean"
  | "object"
  | "array"
  | "null"
  | "geopoint"
  | "reference"

export interface DbQueryFilter {
  field: string
  type?: DbQueryFieldTypes
  operator: DbQueryOperators
  value: string
}
export interface DbQuery {
  path: string
  filter: DbQueryFilter[]
  resultFields: string[]
  subQueries: DbQuery[]
}
export interface QueryBuilderForm {
  _id?: string
  timestamp_created?: string
  timestamp_changed?: string
  name: string
  queries: DbQuery[]
}
export interface FormData {
  client?: Client
  project?: Project
  settings?: UserSettings
  requestPermission?: FormDataRequestPermission
  translation?: Translation
  superAdmin?: SuperAdmin
  slot: Slot
  importText: ImportText
  importFile: ImportFile
  importFileFromUrl: ImportFileFromUrl
  integration: IntegrationForm
  passwordReset: PasswordResetForm
  dummyForm: DummyForm
  queryBuilderForm: QueryBuilderForm
}

export interface RequestPermissionAction {
  timestamp_created?: number
  id?: string
  name: ""
  processed: boolean
  accepted?: boolean
  declined?: boolean
  stoppedByRequester?: boolean
  uid: string
  displayName?: string
  email?: string
  photoURL?: string
  phoneNumber?: string
  providerData?: any
}

/* NAVIGATION */
export interface RoutePropsLocation {
  pathname: string
  search: string
  state: any
  hash: string
  key?: string
}
export interface IAppStateDefaultValues {
  userId: string
  userSession: string
  userRole: chatify.UserRoles
  dialogflowProjectId: string
  displayName: string
  language: string
}
export interface IReducer {
  state: any
  dispatch: (args: chatify.IAction) => void
}
export interface IAction {
  type: string
  payload?: any
}
export type IActionShort = string

export interface ILoginSettings {
  type?: "anonym" | "mail" | "google" | "facebook" | "logout"
  mail?: string
  pwd?: string
  signInWithPopup?: boolean
}

export interface IAppState {
  user?: firebase.User
  authToken?: string
  userSettings?: chatify.UserSettings
  uiSettings?: chatify.UiSettings
  projectSettings?: chatify.Project
  translation?: chatify.Translation[]
  speechStore?: chatify.ChatifyChat.SpeechClass
  chatifySettings?: chatify.ChatifySettingsConstants
  loginSettings?: chatify.ILoginSettings
  loginError?: string
  logoutError?: string
  loadUserSettingsError?: string
  loading?: string[]
}
export interface IAppContext {
  appState: IAppState
  dispatch(args: IAction | IActionShort): void
}
export interface IRouterMachineContext {
  location: string
  id: string
  params: any
  history: any
  routes: chatify.Router.Route[]
  loggedIn: boolean
  userRole: chatify.UserRoles
  [key: string]: any
  /* user: firebase.User
  userSettings: chatify.UserSettings
  translations: chatify.Translation[]
  translationLoaded: boolean
  */
}
export interface IRouterMachine {
  context: IRouterMachineContext
  [key: string]: any
}
export interface MatchParams {
  path: string
  id?: string
  params?: any
  machine: IRouterMachine
}
export interface RouteProps {
  path: string
  pathStructure: string
  id: string
  ids: any
  referrer?: string
  history?: string[]
}
export interface PushMessagePayloadNotification {
  title?: string
  body?: string
  icon?: string
  click_action?: string
}
export interface PushMessagePayload {
  notification?: PushMessagePayloadNotification
  data?: any
  condition?: string
  token?: string
  topic?: string
}
export interface PushMessage {
  active: boolean
  payload: PushMessagePayload
}
export type ProgramMode = "dev" | "prod"
export interface PackageJson {
  name: string
  version: string
  description: string
}
export interface GridSizeSizes {
  xs: number | boolean
  sm: number | boolean
  md: number | boolean
  lg: number | boolean
}
export interface GridSize {
  left: GridSizeSizes
  right: GridSizeSizes
}
export type snackBarVariant = "error" | "warning" | "success" | null
export interface UiSettings {
  page?: string
  chatRightPanelActive?: boolean
  devMode?: boolean
  oldProgramVersion?: boolean
  isPwa?: boolean
  badge?: number
  currentPushMessage?: PushMessage
  loading?: boolean
  chatSize?: ChatSize
  screenWindow?: any
  windowWidth?: number
  windowHeight?: number
  contentLeftWidth?: number
  contentRightWidth?: number
  snackBarTimestamp?: number
  snackBarActive?: boolean
  snackBarVariant?: snackBarVariant
  snackBarContent?: string | JSX.Element
  formData?: FormData
  routeProps?: RouteProps
  getBadgeLabel?: string
  chatifySettingsConstants?: ChatifySettingsConstants
  loggingIn?: boolean
  pageLoaded?: boolean
  componentName?: string
  floatingActionButtonActive?: string
  floatingActionButtonType?: "save" | "add" | "delete" | "edit"
  floatingActionButtonDisabled?: boolean
  floatingActionButtonHandleClick?(args: any): void
  setContentPositionAndSize?(contentPositionAndSize: chatify.PositionAndSize)
  setPageLoaded?(val: boolean): void
  getChatifySettings?({ translate }: { translate: Translate }): void
  onPushMessage?(payload: any): void
  setPushMessage?(pushMessage: chatify.PushMessage): void
  setRouteMatchParams?(params: RouteProps): void
  handleResize?(): void
  handleWindowWidthChange?(): void
  showsnackBar?(val: boolean, text?: string, error?: any): void
  setChatSize?(size: string): void
  setIsPwa?(isPwa: boolean): void
  getProgramMode?(): ProgramMode
  getPackageJson?(): PackageJson
  setLoggingIn?(value: boolean): void
}

export interface TranslationLanguage {
  code: string
  name: string
}

export interface MaintainanceMode {
  intentAdd?: boolean
  intentUpdate?: boolean
}

export interface IntentSettings {
  answerSummaries: boolean
  nouns: boolean
  synonyms: boolean
}

export interface ChatifySettingsConstants {
  supportedTranslationLanguage: TranslationLanguage[]
  maintainanceMode: MaintainanceMode
  intents: IntentSettings
  version: string
}

/* API */
export interface ChatifyError {
  statusCode: number
  name: string
  message: string
  code: string
}

export interface ChatifyResponse extends AxiosResponse {
  error: ChatifyError
  data: any
}

/* Entities */
export interface DialogflowEntitiesStore {
  delEntity({ name }: { name: string }): AxiosPromise<any>
  updateEntity(data: dialogflow.EntityType): AxiosPromise<any>
  addEntity(data: dialogflow.EntityType): AxiosPromise<any>
}

/* INTENTS */
export interface FollowupIntentInfoStructureElement {
  parent: boolean
  level: number
  name: string
  id: string
  parentId: string
}
export type NewSentencePartObject = {
  selectedText: string
  from: number
  to: number
  userDefined: boolean
  entityType: string
  alias: string
}
export type IntentUpdates = { type: string; data: any }

export interface IntentName {
  name: string
}
export interface IntentMessageText {
  text: string[]
}
export interface IntentMessage {
  message: string
  platform?: string
  text: IntentMessageText
}
export interface IntentMessages {
  message: IntentMessage
}
export interface UpdateIntent {
  intent: dialogflow.Intent
}
export interface GetIntent {
  name: string
}
export interface AddIntent {
  intent: dialogflow.Intent
}
export interface DialogflowIntentsStore {
  // loading: boolean
  // intents: any[]
  // active: boolean
  // messages: any[]
  // trainingPhrases: any[]
  composeIntent({
    dialogflowProjectId,
    displayName,
    followupIntentInfo,
    phrases,
    parameters,
    messages,
    webhookState,
    inputContextNames,
    outputContexts
  }: {
    dialogflowProjectId: string
    displayName: string
    followupIntentInfo: dialogflow.FollowupIntentInfo[]
    phrases: dialogflow.TrainingPhrase[]
    parameters: dialogflow.Parameter[]
    messages: IntentMessage[]
    webhookState: string
    inputContextNames: string[]
    outputContexts: dialogflow.Context[]
  }): dialogflow.Intent
  saveIntent({ intent }: { intent: dialogflow.Intent }): AxiosPromise<any>
  saveIntents({ intents }: { intents: dialogflow.Intent[] }): AxiosPromise<any>
  deleteIntent(data: IntentName): AxiosPromise<any>
  getIntent(data: IntentName): AxiosPromise<any>
  updateIntent(data: UpdateIntent): AxiosPromise<any>
  addIntent(data: AddIntent): AxiosPromise<chatify.ChatifyResponse>
}

/* Client */
export interface Client {
  _id?: string
  companyName: string
  contactPersonFirstName: string
  contactPersonLastName: string
  email: string
  street: string
  postalCode: string
  city: string
  country: string
  phone?: string
  fax?: string
  greeting?: string
  uid?: string
  bankName?: string
  bankAccountNr?: string
  iban?: string
  bic?: string
  blz?: string
}

/* DbTable */
export type handleRowClick = ({
  event,
  id,
  data
}: {
  event: any
  id: string
  data?: any
}) => void
export interface DbTableConfigColumnData {
  id: string
  numeric: boolean
  disablePadding: boolean
  label: string
  type?: "email" | "boolean" | "dateTime"
  hidden?: boolean
  element?({
    data: any,
    handleRowClick: handleRowClick
  }: {
    data: any
    handleRowClick: handleRowClick
  }): JSX.Element
}
export interface DbTableConfigFilter {
  id: string
  name: string
  value?: string
}
export interface DbTableConfig {
  path: string
  firestoreFilter?: string[] | any[]
  title: string
  filter?: DbTableConfigFilter[] // boolean | DbTableConfigFilter[]
  // filterField?: string
  columnData: DbTableConfigColumnData[]
  sort?: string
  filterFunction?(dataEntry: any): boolean
}

/* USER */
export type UserRoles = "anonymous" | "editor" | "admin" | "superAdmin"
export interface UserSettings {
  _id?: string
  uid?: string
  session?: string
  userRole?: UserRoles
  chatAudioOutputEnabled?: boolean
  chatDefaultMessage?: string
  chatName?: string
  accounts?: string[]
  clientId?: string
  debug?: boolean
  displayName?: string
  email?: string
  emailVerified?: boolean
  fcmToken?: string
  isAnonymous?: boolean
  language?: string
  projectId?: string
  pushEnabledApp?: boolean
  pushEnabledWeb?: boolean
  rightChatFrameVisible?: boolean
  superAdmin?: boolean
  audioVoice?: string
  timestamp?: number
  addPwaToHomeScreenAsked?: boolean
  projectTopicSubscription?: boolean
  defaultTranslationLanguage?: string
  registrationSetupStepName?: string
  welcomeMailToUserSent?: boolean
  chatifyInfoMailNewAdminUserSent?: boolean
  availableProjects?: Project[]
}

/* CONSTANTS */
interface Default {
  language: string
  contactMail: string
  supportMail: string
}
interface FirebaseCloudMessaging {
  serverkey: string
}
interface Api {
  url: string
  localFirebaseEndpointTestServer?: string
}
interface Chatbot {
  id: string
  name: string
}
export interface Chat {
  company: string
  homepage: string
  logo: string
  defaultFallbackAnswer: string
  sendButton: boolean
  colors: Colors
  welcomeMessage: string
  inputPlaceholder: string
  undefinedAnswer: string
  speakMode: boolean
}
interface Colors {
  primary: string
}
interface Ui {
  rightPanelChat: boolean
}
export interface BreakpointQuery {
  mobile: string
}
export interface Constants {
  userSettings: UserSettings
  projectSettings: Project
  client?: Client
  env?: "development" | "production"
  breakpointQuery: BreakpointQuery
  googleMapApiKey: string
  location?: string
  default: Default
  fcm: FirebaseCloudMessaging
  api: Api
  chatbot: Chatbot
  chat: Chat
  ui: Ui
  dynamicLink: string
  packageName: {
    ios: string
    android: string
  }
  native?: ConstantsNative
  web: ConstantsWeb
  electron?: ConstantsElectron
  highlightColors: string[]
  primaryColor: string
  secondaryColor: string
  grayColor: string
  shadowProps: any
}

export interface ConstantsNative {
  backgroundColor: string
  backgroundColorPage: string
  h1: any
  contentPadding: any
  blackBorder: any
  buttonFlat: any
  listItem: any
}
export interface ConstantsWeb {
  urlAdmin: string
  urlWeb: string
  urlChat: string
  backgroundColor: string
  backgroundColorPage: string
  startPage: string
}
export interface ConstantsElectron {}

/* CHANGELOG */
export interface Changelog {
  _id: string
  timestamp_created?: number
  timestamp: number
  title: string
  content: string
}

/* TODOS */
export interface Todos {
  _id: string
  timestamp: number
  title: string
  content: string
}

/* SPEECH */
export interface SpeechStore {
  sync: SpeechSynthesis
  speechSynthesisAvailable: boolean
  speechRecognitionAvailable: boolean
  speechRecognition: SpeechRecognitionStatic
  textContent: string
  recognition: SpeechRecognition
  speaker: SpeechSynthesisUtterance
  voices: SpeechSynthesisVoice[]
  selectedVoice: number
  speakMode: boolean
  progressIndex: number
  setTextContent(text: string): void
  initializeVoicePopulation(): void
  changeSpeakMode(val: boolean): void
  getVoices(): void
  setVoice(i: number): void
  speak({ text, language }: { text: string; language?: string }): void
}

/* FIREBASE CLOUD MESSAGING */
export interface FirebaseCloudMessagingStore {
  token: string
  setTokenSentToServer: boolean
  permission: boolean
  requestPermission(): void
  showToken(msg: string, err: any): void
  getToken(uid?: string): void
  sendPush(): void
}

export interface FormFieldsConditionResult {
  valid: boolean
  error: string
}
export interface FormFields {
  name: string
  required?: boolean
  min?: number
  email?: boolean
  url?: boolean
  number?: boolean
  date?: boolean
  alpha?: boolean
  error?: string
  condition?({
    name,
    value
  }: {
    name: string
    value: any
  }): FormFieldsConditionResult
}
export interface Form {
  formDataName: string
  valid?: boolean
  dirty?: boolean
  submitted?: boolean
  fields?: FormFields[]
  handleChange?: (event: any) => void
  handleChangeChecked?: (event: any) => void
  handleChangeSelect?: (event: any) => void
  handleSave?: () => void
  check?: (data?: any) => void
}
export interface Input {
  type?:
    | "input"
    | "select"
    | "radio"
    | "checkbox"
    | "switch"
    | "hidden"
    | "inputDateTime"
    | "upload"
    | "intervalFrequency"
    | "color"
  name: string
  label: string
  value?: string
  autoFocus?: boolean
  disabled?: boolean
  checked?: boolean
  multiple?: boolean
  multiline?: boolean
  showChips?: any
  row?: boolean
  // path?: string
  data?: any[]
  dataName?: string
  dataValue?: string
  // sort?: string
  filter?: Filter
  filterList?: FilterList
  pleaseChoose?: boolean
  fullWidth?: boolean
  color?: string
  margin?: "none" | "dense" | "normal"
  error?: string
  required?: boolean
  min?: number
  email?: boolean
  handleChangeAdditionalFunction?(event: any): any
}
export type FirebaseFunctionActionName =
  | "importIntents"
  | "importSingleIntent"
  | "updateAllUser"
  | "deleteFirebaseUser"
  | "syncAllIntentsWithDialogflow"
export interface FirebaseFunctionAction {
  _id?: string
  synced?: boolean
  thirdParty?: string[]
  thirdPartyId?: string[]
  actions: FirebaseFunctionActionName[]
  id?: string
  projectId?: string
  dialogflowProjectId?: string
  data?: any
  userId: string
  from: string
}

export interface ActionImportIntents {
  dialogflowSynced?: boolean
  dialogflowProjectId: string
  projectId: string
  userId: string
  from: string
  intent?: dialogflow.Intent
}

export interface Result {
  success?: boolean
  code?: number
  message?: string
}

/* 
// http://www.nncron.ru/help/EN/working/cron-format.htm
* * * * * *
| | | | | | 
| | | | | +-- Year              (range: 1900-3000)
| | | | +---- Day of the Week   (range: 1-7, 1 standing for Monday)
| | | +------ Month of the Year (range: 1-12)
| | +-------- Day of the Month  (range: 1-31)
| +---------- Hour              (range: 0-23)
+------------ Minute            (range: 0-59) 
*/
export interface DateTimeInterval {
  intervalSeconds:
    | "*"
    | 0
    | 1
    | 2
    | 3
    | 4
    | 5
    | 6
    | 7
    | 8
    | 9
    | 10
    | 11
    | 12
    | 13
    | 14
    | 15
    | 16
    | 17
    | 18
    | 19
    | 20
    | 21
    | 22
    | 23
    | 24
    | 25
    | 26
    | 27
    | 28
    | 29
    | 30
    | 31
    | 32
    | 33
    | 34
    | 35
    | 36
    | 37
    | 38
    | 39
    | 40
    | 41
    | 42
    | 43
    | 44
    | 45
    | 46
    | 47
    | 48
    | 49
    | 50
    | 51
    | 52
    | 53
    | 54
    | 55
    | 56
    | 57
    | 58
    | 59
  intervalMinutes:
    | "*"
    | 0
    | 1
    | 2
    | 3
    | 4
    | 5
    | 6
    | 7
    | 8
    | 9
    | 10
    | 11
    | 12
    | 13
    | 14
    | 15
    | 16
    | 17
    | 18
    | 19
    | 20
    | 21
    | 22
    | 23
    | 24
    | 25
    | 26
    | 27
    | 28
    | 29
    | 30
    | 31
    | 32
    | 33
    | 34
    | 35
    | 36
    | 37
    | 38
    | 39
    | 40
    | 41
    | 42
    | 43
    | 44
    | 45
    | 46
    | 47
    | 48
    | 49
    | 50
    | 51
    | 52
    | 53
    | 54
    | 55
    | 56
    | 57
    | 58
    | 59
  intervalHours:
    | "*"
    | 0
    | 1
    | 2
    | 3
    | 4
    | 5
    | 6
    | 7
    | 8
    | 9
    | 10
    | 11
    | 12
    | 13
    | 14
    | 15
    | 16
    | 17
    | 18
    | 19
    | 20
    | 21
    | 22
    | 23
  intervalDays:
    | "*"
    | 1
    | 2
    | 3
    | 4
    | 5
    | 6
    | 7
    | 8
    | 9
    | 10
    | 11
    | 12
    | 13
    | 14
    | 15
    | 16
    | 17
    | 18
    | 19
    | 20
    | 21
    | 22
    | 23
    | 24
    | 25
    | 26
    | 27
    | 28
    | 29
    | 30
    | 31
  intervalDayOfWeeks: "*" | 1 | 2 | 3 | 4 | 5 | 6 | 7
  intervalMonths: "*" | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12
}

/* export enum ImportType {
  ImportFileFromUrl = "ImportFileFromUrl",
  ImportFile = "ImportFile",
  ImportContent = "ImportContent"
} */

export type ImportType = "ImportFileFromUrl" | "ImportFile" | "ImportContent"

export interface ImportTypeDefaults {
  _id?: string
  dialogflowProjectId?: string
  type?: ImportType
  title: string
  date: number
}

export interface ImportText extends ImportTypeDefaults {
  content: string
}

export interface ImportFileUploadFiles {
  allFiles?: chatify.File[]
  uploadedFiles: chatify.File[]
}

export interface ImportFile extends ImportTypeDefaults {
  // filenames?: string
  uploadFiles: ImportFileUploadFiles
}

export interface ImportFileFromUrl extends ImportTypeDefaults {
  url: string
  // filenames?: string
  dateTimeInterval?: DateTimeInterval
}

export interface Owner {
  projectId: string
  ownerUid: string
}
export type BucketMetadata = Storage.BucketMetadata
export interface File {
  _id?: string
  id: string
  syncedImageDescription?: boolean
  dialogflowProjectId?: string
  projectId?: string
  userId?: string
  messageId?: string
  parentId?: string
  name?: string
  size?: number
  path?: string
  base64Data?: string
  type?: string
  submitted?: boolean
  success?: boolean
  storageInfos?: BucketMetadata
  parent?: File
  timestamp?: number
  error?: any
  imageDescription?: chatify.IVisionImageDescription
  imageDescriptionTranslation?: chatify.TranslatedObject
}

export interface ImportData extends ImportText, ImportFile, ImportFileFromUrl {}

export interface ChartProps {
  title?: string
  data?: Chart.ChartData
  options?: Chart.ChartOptions
  width?: number
  height?: number
}

export interface GeoPoint {
  lat: number
  lng: number
}
export interface GeolocationPosition {
  geohash: string
  geopoint: GeoPoint
}
export type GeolocationService = {
  constructor(projectId: string): void
  getLocation(): GeolocationPosition
}

export interface PushSubscription {
  projectId: string
  topic: string
  token: string
  subscribed: boolean
}

export interface CreatePushMessageNotification {
  title: string
  body: string
  actions?: Array<{
    action: string
    icon?: string
    title: string
  }>
  icon?: string
  topic?: string
  vibrate?: number[]
  condition: string
  projectId: string
  token?: string
  data?: any
}

export interface DbStructureField {
  name: string
  type: DbQueryFieldTypes
}
export interface DbStructure {
  path: string
  fields: DbStructureField[]
}
export type AnimationSpeed = "fast" | "normal" | "slow"
export interface Position {
  x: number
  y: number
}
export interface PositionAndSize {
  x: number
  y: number
  width: number
  height: number
}
export interface AreaProps {
  top?: string | number
  right?: string | number
  left?: string | number
  bottom?: string | number
}
export interface AnimationElementProps {
  opacity?: number
}
export interface AnimationProps {
  draggable: boolean
  dragBounds?: AreaProps
  visible: AnimationElementProps
  hidden: AnimationElementProps
  initialPose?: string
  onChange?: any
  onDragEnd?: any
}
export interface ConversationFlowNode {
  id: string
  name: string
  position?: chatify.Position
  count?: number
  size?: number
  level?: string
  followUps?: ConversationFlowNode[]
  parentNode?: ConversationFlowNode
  oneCountInPercent?: number
}
export interface ConversationFlow {
  totalCount: number
  totalLevels?: number
  oneCountInPercent: number
  size?: number
  nodes: ConversationFlowNode[]
}
export interface SvgProps {
  viewBox?: string
  fill?: string
  path: string
  symbol?: string
  strokeWidth?: string
  stroke?: string
  strokeLinecap?: "butt" | "round" | "square" | "inherit"
  class?: string
  fillOpacity?: number
  strokeOpacity?: number
}
export interface ConversationFlowOptions {
  width: number
  height: number
}

export type Operator = "<" | "==" | ">"
export interface FilterList {
  name: string
  operator: Operator
  values: string[]
}
export interface Filter {
  name: string
  operator: Operator
  value: string
}

export interface User {
  id: string
  name?: string
}

export type chatifyMessageObjectLanguageCodes =
  | "de-DE"
  | "en-US"
  | "fr-FR"
  | "es-ES"
  | "it-IT"
  | "pl-PL"
  | "nl-NL"

export interface chatifyMessageObjectType {
  type:
    | "typing"
    | "text"
    | "video"
    | "image"
    | "keepAlive"
    | "error"
    | "filesUpload"
    | "imageLabels"
}

export type DeeplInput = {
  text: string
  from?: string
  to?: string
}
export interface DeeplOutputTranslations {
  detected_source_language: string
  text: string
}
export interface DeeplOutput {
  translations: DeeplOutputTranslations[]
}
export interface TranslatedObject {
  translations: DeeplOutputTranslations[]
  translationPreferred: string
  translatedTo: string
  translatedFrom: string
  error: string
}
export interface MessageObject {
  _id?: string
  projectId: string
  dialogflowProjectId: string
  dialogflowSynced: boolean
  timestamp: number
  userSession: string
  userId: string
  chatInputMessageId: string
  chatInputOrOutputMessage?: "input" | "output"
  syncedBase64Data?: boolean
  syncedImageDescription?: boolean
  syncedUploadsDatabase?: boolean
  from: User
  to: User
  txt: string
  sent: boolean
  pushed: boolean
  sending?: boolean
  sentWithEndpoint: boolean
  // languageCode?: chatifyMessageObjectLanguageCodes
  type: chatifyMessageObjectType["type"]
  intentDisplayName?: string // dialogflow.Intent
  intentDetectionConfidence?: number
  translated?: TranslatedObject
  translationSettingsLanguage?: string
  /* detected_source_language?: string
  translatedTo?: string */
  data?: any
  base64Data?: string[]
  uploadedFiles?: chatify.File[]
  customPayload?: {}
}
/* }
declare function chatify(): any
export = chatify */
export as namespace chatify
