import { MachineConfig } from "xstate"

export declare namespace Router {
  export interface InvokeTarget {
    target?: string
    actions?: any
    error?: any
  }
  export interface Invoke {
    id?: string
    onDone?: InvokeTarget
    onError?: InvokeTarget
    src?(ctx?: any, event?: any): any
  }
  export interface Route {
    location: string
    component?: string
    activities?: any
    invoke?: Invoke
    states?: any
    initial?: string
    id: string
    actions?: string | any[]
    cond?(ctx: any, event: any): void
  }
  export interface Event {
    internal?: boolean
    target?: string
    actions?: string[]
    cond?(ctx: any, event: any): void
  }
  export interface Events {
    [eventName: string]: Event | Event[] | any
  }
  export interface IState {
    id?: string
    initial?: string
    context?: any
    states?: IStates
    initiaEvent?: any
    name?: string
    on?: Events
    invoke?: Invoke
    onEntry?: any
    onExit?: any
    type?: "parallel" | "history"
  }
  export interface IStates {
    [stateName: string]: IState
  }
  export interface Params {
    [id: string]: string
  }
  export interface InitalEventWithUpdatedContext {
    initialEvent: string
    updatedContext: any
  }
  export interface IRouterConfig {
    routes: Route[]
    initial: string
    falseConditionFallbackState?: string
    context?: any
    activities?: any
  }
  /* export interface IMachineConfig {
      id?: string
      key?: string
      initial?: string
      context?: any
      activities?: any
      states?: IStates
    } */
  export interface IMachine {
    name: string
    config: IState
    actions?: any
    activities?: any
  }
  export interface IRouterSettings extends IRouterConfig {
    config: MachineConfig<any, any, any> // IState
  }
}
